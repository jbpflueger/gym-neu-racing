


# Install

```bash
git clone ...

python -m virtualenv venv
source venv/bin/activate

python -m pip install -e .
```

# Run

```bash
python examples/example.py
```