from glob import glob

from setuptools import find_packages, setup

setup(
    name="gym_neu_racing",
    version="0.0.1",
    install_requires=["gymnasium>=0.26.1", "numpy", "mypy"],
    packages=find_packages(),
    package_data={
        "": [
            "_static/maps/*.png",
        ]
    },
)
