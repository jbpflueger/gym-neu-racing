from gymnasium import register

register(
    id="gym_neu_racing/NEURacing-v0",
    entry_point="gym_neu_racing.envs:NEURacingEnv",
)

register(
    id="gym_neu_racing/NEUEmptyWorld-v0",
    entry_point="gym_neu_racing.envs:NEUEmptyWorldEnv",
)
