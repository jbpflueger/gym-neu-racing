"""2D Lidar sensor with Gaussian noise added to range readings."""

import numpy as np

from .lidar_2d import Lidar2D


class Lidar2DGaussian(Lidar2D):
    """2D Lidar sensor with Gaussian noise added to range readings."""

    def __init__(
        self,
        static_map,
        num_beams=16,
        range_resolution=0.1,
        range_limits=np.array([0, 10]),
        angle_limits=np.array([-np.pi / 2, np.pi / 2]),
        range_variance=0.1**2,
    ):
        super().__init__(
            static_map, num_beams, range_resolution, range_limits, angle_limits
        )
        self.range_variance = range_variance
        self.range_std = np.sqrt(range_variance)

    def step(self, current_state: np.ndarray) -> dict:
        """Query Lidar2D.step, then add Gaussian noise to the ranges."""
        sensor_data = super().step(current_state)

        ranges = np.random.normal(sensor_data["ranges"], self.range_std)

        sensor_data["ranges"] = ranges

        return sensor_data
