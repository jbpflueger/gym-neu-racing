"""Add gaussian noise to the perfect state observation."""

import numpy as np

from .state_feedback import StateFeedback


class NoisyStateFeedback(StateFeedback):
    """Add gaussian noise to the perfect state observation."""

    def __init__(self, pose_variance=np.array([0.1**2, 0.1**2, 0.1**2])):
        super().__init__()
        self.pose_variance = pose_variance

    def step(self, current_state: np.ndarray) -> np.ndarray:
        current_state = super().step(current_state)
        noisy_state = np.random.normal(current_state, self.pose_variance)
        return noisy_state
