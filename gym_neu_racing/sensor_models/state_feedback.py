from gymnasium import spaces

from .sensor_model import SensorModel


class StateFeedback(SensorModel):
    def __init__(self):
        super().__init__()

        self.observation_space = spaces.Box(-10, 10, shape=(3,), dtype=float)

    def step(self, current_state):
        return current_state
