"""2D Lidar sensor model for EECE 5550 simulator."""

import numpy as np
from gymnasium import spaces

from .sensor_model import SensorModel


class Lidar2D(SensorModel):
    """2D LaserScan based on map of the environment

    :param num_beams: (int) how many beams/rays should be in the laserscan
    :param range_resolution: (float) distance to increment along each beam
                                (meters)
    :param range_limits: (np.array) min, max range per beam (meters)
    :param angle_limits: (np.array) angle of first and last beam, relative to
                                    agent's current heading (radians)
    """

    def __init__(
        self,
        static_map,
        num_beams=16,
        range_resolution=0.1,
        range_limits=np.array([0, 10]),
        angle_limits=np.array([-np.pi / 2, np.pi / 2]),
    ):
        super().__init__()
        self.static_map = static_map
        self.num_beams = num_beams
        self.range_resolution = range_resolution
        self.range_limits = range_limits
        self.angle_limits = angle_limits

        self.angles = np.linspace(
            self.angle_limits[0], self.angle_limits[1], self.num_beams
        )

        self.ranges = np.arange(
            self.range_limits[0], self.range_limits[1], self.range_resolution
        )

        self.observation_space = spaces.Dict(
            {
                "angles": spaces.Box(
                    -np.pi, np.pi, shape=(self.num_beams,), dtype=float
                ),
                "ranges": spaces.Box(
                    self.range_limits[0],
                    np.inf,
                    shape=(self.num_beams,),
                    dtype=float,
                ),
            }
        )

    def step(self, current_state: np.ndarray) -> dict:
        """Use top-down map to ray-trace for obstacles."""

        # assume sensor & robot frames are coincident
        heading = current_state[2]
        position = current_state[0:2]

        # Get matrix of the (x, y) along each beam in the world frame
        # beam_pts_in_world_frame.shape: (num_beams, num_ranges, 2 [x,y])
        angles = self.angles + heading
        ranges = self.ranges
        angles_ranges_mesh = np.meshgrid(angles, ranges)
        angles_ranges = np.dstack(
            [angles_ranges_mesh[0], angles_ranges_mesh[1]]
        )
        beam_pts_in_world_frame = np.tile(
            position, (len(angles), len(ranges), 1)
        ).astype(np.float64)
        beam_pts_in_world_frame[:, :, 0] += (
            angles_ranges[:, :, 1] * np.cos(angles_ranges[:, :, 0])
        ).T
        beam_pts_in_world_frame[:, :, 1] += (
            angles_ranges[:, :, 1] * np.sin(angles_ranges[:, :, 0])
        ).T

        beam_pts_in_map_indices, in_maps = (
            self.static_map.world_coordinates_to_map_indices(
                beam_pts_in_world_frame
            )
        )
        # pylint:disable=singleton-comparison
        beam_pts_in_map_indices[in_maps == False] = 0  # noqa: E712
        # pylint:enable=singleton-comparison

        # Lidar hits count if that beam is within the map boundaries and the
        # cell is occupied
        lidar_hits = np.logical_and.reduce(
            (
                self.static_map.static_map[
                    beam_pts_in_map_indices[:, :, 0],
                    beam_pts_in_map_indices[:, :, 1],
                ],
                in_maps,
            )
        )

        # Each range is the first pt along that beam that had a hit
        ranges = self.ranges[(lidar_hits != 0).argmax(axis=1)]

        # For any beams that don't get a hit, report np.inf as the range
        # (per ROS REP 117)
        max_range_beams = np.all(lidar_hits == 0, axis=1)
        ranges[max_range_beams] = np.inf

        # angles, ranges reported in sensor frame
        sensor_data = {
            "ranges": ranges,
            "angles": self.angles,
        }

        return sensor_data
