"""Discrete-time bicycle kinematic model for 2D robot simulator."""

import numpy as np
from gymnasium import spaces

from .motion_model import MotionModel


class Bicycle(MotionModel):
    """Discrete-time bicycle kinematic model for 2D robot simulator."""

    def __init__(self):
        self.action_space = spaces.Box(-100, 100, shape=(2,), dtype=float)
        self.L = 1.0  # pylint: disable=invalid-name
        super().__init__()

    def step(self, current_state, action, dt=0.1):
        # current_state = np.array([x, y, theta])
        # action = np.array([speed, steering_angle])

        next_state = np.empty((3,))
        next_state[0] = current_state[0] + dt * action[0] * np.sin(
            current_state[2]
        )
        next_state[1] = current_state[1] + dt * action[0] * np.cos(
            current_state[2]
        )
        next_state[2] = current_state[2] + dt * (action[0] / self.L) * np.tan(
            action[1]
        )

        return next_state
