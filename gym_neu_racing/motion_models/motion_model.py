"""Abstract class for modeling the motion of a 2D mobile robot."""

class MotionModel:  # noqa: E302
    """Abstract class for modeling the motion of a 2D mobile robot."""

    def __init__(self):
        pass

    def step(self, current_state, action, dt=0.1):
        """Move 1 timestep forward w/ kinematic model, x_{t+1} = f(x_t, u_t)"""
        raise NotImplementedError
