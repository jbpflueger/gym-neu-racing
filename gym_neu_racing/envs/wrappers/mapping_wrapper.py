import numpy as np
from gymnasium import ObservationWrapper


class MappingWrapper(ObservationWrapper):
    def observation(self, observation: dict) -> np.ndarray:
        return {"state": observation["state"], "lidar": observation["lidar"]}
