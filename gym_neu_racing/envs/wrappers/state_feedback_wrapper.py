import numpy as np
from gymnasium import ObservationWrapper


class StateFeedbackWrapper(ObservationWrapper):
    def observation(self, observation: dict) -> np.ndarray:
        return observation["state"]
