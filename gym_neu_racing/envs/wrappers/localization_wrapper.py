import numpy as np
from gymnasium import ObservationWrapper


class LocalizationWrapper(ObservationWrapper):
    def observation(self, observation: dict) -> np.ndarray:
        return observation["lidar"]
