"""Gym env for 2D simulation of mobile robots on racetracks (NEU EECE 5550)."""

import os
from typing import Any, Optional

import gymnasium as gym
import numpy as np
from gymnasium import spaces

import gym_neu_racing.envs.map as map  # pylint:disable=redefined-builtin
import gym_neu_racing.motion_models as motion_models
import gym_neu_racing.sensor_models as sensor_models


class NEURacingEnv(gym.Env):
    """Gym env for 2D simulation of mobile robots on racetracks."""

    def __init__(
        self, map_name: str = "circle", render_mode: Optional[str] = None
    ):
        self.render_mode = render_mode
        dir_path = os.path.dirname(os.path.realpath(__file__))

        self.laps_left = 2

        map_dir = os.path.join(dir_path, "..", "_static", "maps")
        if map_name == "square_shortcut":
            self.finish_line = np.array([[0.0, -3.2], [0.0, -4.9]])
            self.center = np.array([0.0, 0.0])
        elif map_name == "circle":
            map_filename = os.path.join(map_dir, "circle.png")
            self.finish_line = np.array([[0.0, -1.5], [0.0, -5.0]])
            self.center = np.array([0.0, 0.0])
        else:
            raise NotImplementedError

        self.map = map.Map(10, 10, 0.1, map_filename=map_filename)

        self.motion_model = motion_models.Unicycle()
        self.sensor_models = {
            "state": sensor_models.StateFeedback(),
            # "lidar": sensor_models.Lidar2D(self.map),
        }

        self.observation_space = spaces.Dict(
            {
                key: sensor.observation_space
                for (key, sensor) in self.sensor_models.items()
            }
        )
        self.action_space = self.motion_model.action_space

        self.state = None

    @property
    def motion_model(self):
        """Get motion_model."""
        return self._motion_model

    @motion_model.setter
    def motion_model(self, motion_model_to_use):
        self._motion_model = motion_model_to_use
        self.action_space = motion_model_to_use.action_space

    @property
    def sensor_models(self):
        """Get sensor_models."""
        return self._sensor_models

    @sensor_models.setter
    def sensor_models(self, sensor_models_to_use):
        self._sensor_models = sensor_models_to_use
        self.observation_space = spaces.Dict(
            {
                key: sensor.observation_space
                for (key, sensor) in sensor_models_to_use.items()
            }
        )

    def _get_obs(self):
        observation = {}
        for key, sensor in self.sensor_models.items():
            observation[key] = sensor.step(self.state.copy())

        return observation

    def _get_info(self):
        return {}

    def reset(
        self, *, seed: Optional[int] = None, options: Optional[dict] = None
    ) -> tuple[dict[str, Any], dict]:
        # We need the following line to seed self.np_random
        super().reset(seed=seed)

        self.state = np.array([0.81, -4.0, np.pi])
        self.state[0] += np.random.uniform(-0.3, 0.3)
        self.state[1] += np.random.uniform(-0.5, 0.5)
        self.state[2] += np.random.uniform(-0.5, 0.5)

        assert (
            self.check_if_agent_in_free_space()
        ), "Agent initialized outside map or in occupied space."

        observation = self._get_obs()
        info = self._get_info()

        return observation, info

    def step(self, action):
        prev_state = self.state.copy()
        self.state = self.motion_model.step(prev_state, action)
        observation = self._get_obs()

        self.update_lap_count(prev_state, self.state)

        # An episode is done iff the agent has reached the target
        if not self.check_if_agent_in_free_space():
            # Crashed into occupied space
            reward = -100
            terminated = True
        elif self.laps_left == 0:
            # Crossed the finish line
            reward = 1
            terminated = True
        else:
            reward = 0
            terminated = False
        info = self._get_info()

        return observation, reward, terminated, False, info

    def check_if_agent_in_free_space(self):
        """Confirm that agent starts in free space inside map."""
        grid_coords, in_map = self.map.world_coordinates_to_map_indices(
            self.state[0:2]
        )
        if not in_map:
            print("Outside of map boundaries.")
            return False
        if self.map.static_map[grid_coords[0], grid_coords[1]]:
            print("In occupied space.")
            return False
        return True

    def update_lap_count(self, prev_state, current_state) -> None:
        """Check if agent just crossed finish line (in either direction)."""
        x1, y1 = prev_state[0:2]
        x2, y2 = current_state[0:2]
        x3, y3 = self.finish_line[0, :]
        x4, y4 = self.finish_line[1, :]
        denom = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
        if denom == 0:
            # unsure what we should do in this case...
            return
        t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / denom
        u = ((x1 - x3) * (y1 - y2) - (y1 - y3) * (x1 - x2)) / denom
        if t > 0 and t <= 1 and u >= 0 and u <= 1:
            print("lap completed!")

            prev_state_to_inner_finish_pt = (
                prev_state[0:2] - self.finish_line[0, :]
            )
            current_state_to_inner_finish_pt = (
                current_state[0:2] - self.finish_line[0, :]
            )
            prev_theta_to_inner_finish_pt = np.arctan2(
                prev_state_to_inner_finish_pt[1],
                prev_state_to_inner_finish_pt[0],
            )
            current_theta_to_inner_finish_pt = np.arctan2(
                current_state_to_inner_finish_pt[1],
                current_state_to_inner_finish_pt[0],
            )

            if (
                current_theta_to_inner_finish_pt
                < prev_theta_to_inner_finish_pt
            ):
                print("clockwise -> fwd lap completed")
                self.laps_left -= 1
            else:
                print("ccw -> bwd lap completed")
                self.laps_left += 1

    def render(self):
        raise NotImplementedError
