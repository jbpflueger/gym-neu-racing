"""Gym env for 2D simulation of mobile robots (used for NEU EECE 5550)."""

from typing import Any, Optional

import gymnasium as gym
import numpy as np
from gymnasium import spaces

import gym_neu_racing.motion_models as motion_models
import gym_neu_racing.sensor_models as sensor_models


class NEUEmptyWorldEnv(gym.Env):
    """Gym env for 2D simulation of mobile robots (used for NEU EECE 5550)."""

    def __init__(self, render_mode: Optional[str] = None):
        self.render_mode = render_mode
        self.motion_model = motion_models.Unicycle()
        self.sensor_models = {
            "state": sensor_models.StateFeedback(),
        }

        # The system starts in a random state within these state limits
        self.init_state_range = np.array(
            [
                [-2.0, 2.0],
                [-2.0, 2.0],
                [-np.pi, np.pi],
            ]
        )

        # The goal position will be within this box, but outside a circle
        # around origin
        self.goal_pos_range = np.array(
            [
                [-10.0, 10.0],
                [-10.0, 10.0],
            ]
        )
        self.goal_min_radius = 4.0

        self.goal_threshold = 0.5

        self.dt = 0.1

        self.goal = None
        self.state = None

    @property
    def motion_model(self):
        """Get motion_model."""
        return self._motion_model

    @motion_model.setter
    def motion_model(self, motion_model_to_use):
        self._motion_model = motion_model_to_use
        self.action_space = motion_model_to_use.action_space

    @property
    def sensor_models(self):
        """Get sensor_models."""
        return self._sensor_models

    @sensor_models.setter
    def sensor_models(self, sensor_models_to_use):
        self._sensor_models = sensor_models_to_use
        self.observation_space = spaces.Dict(
            {
                key: sensor.observation_space
                for (key, sensor) in sensor_models_to_use.items()
            }
        )

    def _get_obs(self) -> dict[str, Any]:
        if self.state is None:
            raise ValueError(
                "You need to reset the environment to initialize self.state."
            )
        observation = {}
        for key, sensor in self.sensor_models.items():
            observation[key] = sensor.step(self.state.copy())

        return observation

    def _get_info(self) -> dict[str, Any]:
        return {}

    def _get_goal(self) -> np.ndarray:
        return np.random.uniform(
            low=self.goal_pos_range[:, 0], high=self.goal_pos_range[:, 1]
        )

    def reset(
        self, *, seed: Optional[int] = None, options: Optional[dict] = None
    ) -> tuple[dict[str, Any], dict]:
        # We need the following line to seed self.np_random
        super().reset(seed=seed)

        # Set goal within the goal limits but outside circle around origin
        goal = self._get_goal()
        while goal[0] ** 2 + goal[1] ** 2 < self.goal_min_radius**2:
            goal = self._get_goal()
        self.goal = goal

        # Reset the state of the environment to an initial state
        self.state = np.random.uniform(
            low=self.init_state_range[:, 0], high=self.init_state_range[:, 1]
        )

        observation = self._get_obs()
        info = self._get_info()

        return observation, info

    def step(self, action):
        self.state = self.motion_model.step(
            self.state.copy(), action, dt=self.dt
        )
        observation = self._get_obs()

        # An episode is done iff the agent has reached the target
        if self.check_if_at_goal():
            # Reached the goal coordinate
            reward = 1
            terminated = True
        else:
            reward = 0
            terminated = False
        info = self._get_info()

        return observation, reward, terminated, False, info

    def check_if_at_goal(self) -> bool:
        """Check if euclidean distance to goal is below threshold."""
        return (
            np.linalg.norm(self.state[0:2] - self.goal) <= self.goal_threshold
        )

    def render(self):
        raise NotImplementedError
